<?php


namespace Ucc\Controllers;


use Ucc\Services\QuestionService;
use Ucc\Session;

class QuestionsController extends Controller
{

    private QuestionService $questionService;

    public function __construct(QuestionService $questionService)
    {
        parent::__construct();
        $this->questionService = $questionService;
    }

    public function beginGame(): bool
    {
        $name = $this->requestBody->name ?? null;
        if (empty($name)) {
            return $this->json('You must provide a name', 400);
        }
        Session::set('name', $name);
        Session::set('questionCount', 1);
        Session::set('points', 0);
        //TODO Get first question for user
        $questions = $this->questionService->getRandomQuestions(1);
        $questions = array_map(function ($q) {
            unset($q['correctAnswer']);
            return $q;
        }, $questions);
        //TODO: store question in session to avoid repeat same questions

        //  $questions_id = $this->questionService->getQuestionsId();
        return $this->json(['question' => $questions], 201);
    }

    public function answerQuestion(int $id): bool
    {
        if (Session::get('name') === null) {
            return $this->json('You must first begin a game', 400);
        }

        if ((int)Session::get('questionCount') > 4) {
            $name = Session::get('name');
            $points = Session::get('points');
            Session::destroy();
            return $this->json(['message' => "Thank you for playing {$name}. Your total score was: {$points} points!"]);
        }

        $answer = $this->requestBody->answer ?? null;
        if (empty($answer)) {
            return $this->json('You must provide an answer', 400);
        }

        //TODO: Check answer and increment user's points. Reply with a proper message
        $answer_points = $this->questionService->getPointsForAnswer($id, $answer);
        Session::set('points', Session::get('points') + $answer_points);

        $message = $answer_points ? 'Good Answer ' . Session::get('points') . ' points win' : 'Wrong answer ' . Session::get('points') . '';
        $question = null;
        $questions = $this->questionService->getRandomQuestions(1);
        $questions = array_map(function ($q) {
            unset($q['correctAnswer']);
            return $q;
        }, $questions);
        Session::set('questionCount', Session::get('questionCount') + 1);
        return $this->json(['message' => $message, 'question' => $questions]);
    }
}