<?php


namespace Ucc\Services;


use JsonMapper;
use KHerGe\JSON\JSON;

class QuestionService
{
    const QUESTIONS_PATH = __DIR__ . '/../../questions.json';

    private JSON $json;
    private JsonMapper $jsonMapper;

    public function __construct(JSON $json, JsonMapper $jsonMapper)
    {
        $this->json = $json;
        $this->jsonMapper = $jsonMapper;
    }

    private function getQuestionJson(): array
    {
        return json_decode(file_get_contents(self::QUESTIONS_PATH), true);
    }

    public function getRandomQuestions(int $count = 5): array
    {
        $questions = $this->getQuestionJson();
        $questions_keys = array_rand($questions, $count);
        //TODO: Get {$count} random questions from JSON
        return array_map(function ($k) use ($questions) {
            return $questions[$k];
        }, is_int($questions_keys) ? [$questions_keys] : $questions_keys);
    }

    public function getPointsForAnswer(int $id, string $answer): int
    {
        //TODO: Calculate points for the answer given
        foreach ($this->getQuestionJson() as $question) {
            if ($question['id'] == $id && $question['correctAnswer'] == $answer) {
                return $question['point'];
            }
        }
        return 0;
    }

    public function getQuestionsId(): array
    {
        return array_column($this->getQuestionJson(), 'id');
    }
}